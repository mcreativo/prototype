<?php


trait PrototypeImpl
{
    /**
     * @return Product
     */
    public function copy()
    {
        return clone $this;
    }
} 