<?php


interface Prototype
{
    /**
     * @return Prototype
     */
    public function copy();
} 