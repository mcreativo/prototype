<?php


class Creator
{

    /**
     * @var Prototype
     */
    protected $prototype;

    /**
     * @return Product
     */
    function createProduct()
    {
        return $this->prototype->copy();
    }

    /**
     * @param Prototype $prototype
     */
    public function setPrototype($prototype)
    {
        $this->prototype = $prototype;
    }


}