<?php

function __autoload($class_name)
{
    include $class_name . '.php';
}

$opt = getopt('', array('type:'));
$type = isset($opt['type']) ? ($opt['type'] == 2 ? 2 : 1) : 1;


$prototypeOfProduct1 = new Product1();
$prototypeOfProduct2 = new Product2();


$creator = new Creator();
$creator->setPrototype(${'prototypeOfProduct' . $type});


include 'Client.php';

var_dump($product);